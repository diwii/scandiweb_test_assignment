/**
 * Notification message.
 * Message disappears after 5seconds.
 */
function setStatus(text, error=false)
{
    const statusField = document.querySelector('#status');
    let message = document.createElement("p");

    message.className = "message";
    if (error) message.className += " error";
    message.innerText = text;
    
    statusField.append(message);

    setTimeout(()=>{
        message.remove();
    }, 5000);
}