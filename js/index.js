// Pick DOM nodes.
const massDelete = document.querySelector('#massDelete');
massDelete.addEventListener('click', removeSelected);

function removeSelected()
{
    // Select checked checkboxes.
    const marked = document.querySelectorAll('.markOfDeath:checked');

    if (marked.length > 0) {
        // Create list of checkbox values.
        let list = [];
        for (let mark of marked.values()) {
            list.push(mark.value);
        }

        POST(list); // Send to server.
    } else {
        setStatus('Please select product to delete.', true);
    }
}

/**
 * POST payload to server.
 * If response code is 204, remove selected items from DOM.
*/
function POST(payload)
{
    let headers = new Headers(
        {'content-type': 'application/json'}
    );

    const options = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(payload)
    };

    fetch('/product/massDelete', options)
        .then(response => {
            // If database deletes record.
            if (response.status === 204) {
                let removedProducts = [];
                // Remove deleted products from DOM.
                for (let id of payload){
                    // Select product container.
                    let product = document.querySelector(`#record-${id}`);
                    // Select product title.
                    let productName = document.querySelector(`#record-${id} .product-title`).innerText;
                    // Remove node.
                    product.remove();
                    // Add to removed list.
                    removedProducts.push(productName);
                }
                // Push notification.
                let text = `${removedProducts.join(', ')} deleted.`;
                setStatus(text);
            } else if (response.ok) {
                // If server sent errors.
                response.text().then(text => {setStatus(text, true)});
            } else {
                return Promise.reject('Database response not acceptable.');
            }
        })
        .catch(error => {
            console.log('Payload Error!');
            console.error(error);
        });
}