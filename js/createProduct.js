// Create objects.
const prod = new Product();
const fields = new Fields();

// Pick DOM node.
const selectField = document.querySelector('#productType');
selectField.addEventListener('change', getProductType);

function getProductType()
{
    fields.show(this.value); // Should put this into prod object?
    prod.selectType(this);
}

/**
 * Validation
 */
const form = document.querySelector('#createProduct');
// Prevent default browser validation
form.noValidate = true;

/**
 * Validate on submit.
 */
form.addEventListener('submit', function(event){
    event.preventDefault();

    if (prod.validateAll(form)){
        prod.POST();
    }
});

/**
 * Validate on blur.
 */
for (let input of form.elements) {
    // Skip if type submit
    if (input.type.toLowerCase() === 'submit') continue;

    input.addEventListener('blur', function(){
        prod.set(this);
    });
}

/**
 * Save
 * Post request to server.
 */
const saveButton = document.querySelector('#save');
saveButton.addEventListener('click', postRequest);

function postRequest()
{
    if (prod.validateAll(form)){
        prod.POST();
    }
}

/**
 * Do I need this?? It fires event on load for example if
 * submit button is pressed and user returns back.
 */
window.addEventListener('load', function(){

    if (selectField.selectedIndex > 0) {
        let event = new Event('change');
        selectField.dispatchEvent(event);
    }
});