class Type
{
    properties()
    {
        return Object.keys(this);
    }
    type()
    {
        return this.constructor.name.toLowerCase();
    }
}
class Dvd extends Type
{
    dvdSize = new ProductProperty;
}
class Book extends Type
{
    bookWeight = new ProductProperty;
}
class Furniture extends Type
{
    furnitureHeight = new ProductProperty;
    furnitureWidth = new ProductProperty;
    furnitureDepth = new ProductProperty;
}