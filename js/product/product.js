class Product
{
    name = new ProductProperty;
    price = new ProductProperty;

    sku = new ProductProperty(undefined, [
        new Critery('notBeginWith', '0', 'SKU code should not start with 0'),
        new Critery('unique', this.productColumn('sku'), 'SKU code already taken.')
    ]);

    type = new ProductProperty(undefined, [
        new Critery('required', null, 'Please select product type.')
    ]);

    /**
     * Type methods return object of type
     */
    dvd()
    {
        return new Dvd();
    }
    book()
    {
        return new Book();
    }
    furniture()
    {
        return new Furniture();
    }

    /**
     * Select product type from type methods.
     */
    selectType(type)
    {
        // Set type value.
        this.set(type);
        // Create object of type.
        let obj = this[this.type.value]();
        // Add specific type properties to product.
        for (let prop of obj.properties()){
            // Skip if property exists.
            if (this.propExists(prop)) continue;
            this[prop] = obj[prop];
        }
    }

    /**
     * Set property value.
     */
    set(input)
    {
        if (this.validate(input)) {
            this[input.name].set(input);
        }
    }

    /**
     * Check if property exists.
     */
    propExists(property)
    {
        let properties = Object.keys(this);
        return properties.includes(property);
    }

    /**
     * Check for old values.
     */
    old(input)
    {
        if (this.propExists(input.name))
            if (this[input.name].value) return this[input.name].value;
        return '';
    }

    /**
     * Validate single input field.
     */
    validate(input)
    {
        // Critery check.
        if (this[input.name].checkCriterion(input)) {
            // Reset Custom Error
            input.setCustomValidity('');
        }
        // Check if passes The Constraint Validation API
        if (input.validity.valid) {
            // Remove error text.
            this.getErrorLabel(input).innerHTML = '';
            return true;
        }

        // Set error message
        this.error(input);
    }

    /**
     * Validate all input fields.
     */
    validateAll(form)
    {
        let failed = [];
        let payload = {};

        for (let input of form) {
            if (input.disabled
                || input.type === 'submit'
                || input.type === 'fieldset') continue;
            
            // if (this.validate(input)) continue;
            if (this.validate(input)) {
                payload[input.name] = input.value;
            } else {
                failed.push(input);
            }
        }

        if (failed.length === 0) {
            this.payload = payload;
            return true;
        }

        setStatus('Please correct errors.', true);
        return false;
    }

    /**
     * Display Error messages.
     */
    error(input)
    {
        // Add error handling
        this.getErrorLabel(input).innerHTML = input.validationMessage;

        // If Product object contains old values
        input.value = this.old(input);
    }

    /**
     * Return input error label.
     */
    getErrorLabel(input)
    {
        let errorLabel = Array.from(input.labels).find(label => {
                return label.className === 'error';
        });
        return errorLabel;
    }

    /**
     * POST payload to server.
     * If response code is 201 Created, redirect to response header Location.
     * If response is with errors array, display them on control fields.
     */
    POST()
    {
        let headers = new Headers(
            {'content-type': 'application/json'}
        );

        const options = {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(this.payload)
        };

        fetch('/product/add', options)
            .then(response => {
                if (response.status === 201) {
                    // Redirect
                    let uri = response.headers.get('Location');
                    window.location.href = uri;
                    // console.log('I will redirect to '+ uri);

                // If we get validation errors from server.
                } else if (response.ok && response.headers.get('content-type') === 'application/json') {
                    response.json()
                        .then(data => {
                        // Loop through error array.
                        for (let inputName in data.error) {
                            // Get error message.
                            let errorMessage = data.error[inputName];
                            // Get input field.
                            let input = form.elements.namedItem(inputName);
                            // Set error message.
                            input.setCustomValidity(errorMessage);
                            // Display error message.
                            this.error(input);   
                        }
                    });
                } else {
                    return Promise.reject('Dis broken, no JSON or !OK');
                }
            })
            .catch(error => {
                console.log('Payload Error!');
                console.error(error);
                setStatus('Server error', true);
            });
    }

    /**
     * Returns Promise with all Product records.
     */
    async fetchProduct()
    {
        const response = await fetch('/product/list/json');
        if (response.ok && response.headers.get('content-type') === 'application/json') {
            const products = await response.json();
            return products;
        } else {
            return Promise.reject('No JSON');
        }
    }

    /**
     * Returns array of single column from Product records.
     */
    productColumn(columnName)
    {
        let column = [];

        this.fetchProduct().then(products => {
            products.forEach(product => {
                column.push(product[columnName]);
            });
        })
        .catch(error => {
            console.log('Fetch from DB error.');
            console.error(error);
        });

        return column;
    }
}