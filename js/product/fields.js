class Fields
{
    visible;

    /**
     * Show product type attributes.
     */
    show(type)
    {
        // Hide previously opened product type
        this.hide();
        // Store product type, to check if it is open
        this.visible = type;
        // Pick DOM nodes
        let fieldset = document.querySelector('#'+this.visible);
        let inputs = document.querySelectorAll('#'+this.visible+' input')

        // Enable input fields.
        this.disabled(inputs, false);
        // Set input fields visible.
        fieldset.style.display = 'grid';
    }
    /**
     * Hide product type attributes.
     */
    hide()
    {
        // Check if 
        if (!this.visible) return;
        // Pick DOM nodes.
        let div = document.querySelector('#'+this.visible);
        let inputs = document.querySelectorAll('#'+this.visible+' input')
        
        // Disable input fields.
        this.disabled(inputs, true);
        // Hide input fields.
        div.style.display = 'none';
    }
    /**
     * Set input enabled or disabled
     */
    disabled(inputs, bool)
    {
        for (let input of inputs) {
            input.disabled = bool;
        }
    }
}