class ProductProperty
{
    value;
    criterions;
    valid = false;
    messages;

    constructor (value, criterions)
    {
        this.value = value;
        this.criterions = criterions;
    }
    /**
     * Set value for property.
     */
    set(input)
    {
        // // If Critery check fails.
        // if(!this.checkCriterion(input)) return false;
        
        // // If Critery check passes
        // // Reset Custom Error
        // input.setCustomValidity('');

        
        // Set value to property.
        this.value = input.value;

        this.valid = true;
    }

    /**
     * Criterion check loop. 
     */
    checkCriterion(input)
    {
        // Check if property has criterions.
        if(!this.criterions) return true;

        // Collect failures
        let bag = [];
        // Loop through critery list
        for (let critery of this.criterions){
            // Check if input passes critery.
            if (critery.check(input)) continue;
            // Else add critery to failure bag.
            bag = critery.name;
        }
        // If failure bag is empty.
        if (bag.length === 0) return true;

        // temp bag:
        this.messages = bag;

        // Else return false.
        return false;
    }
    /**
     * Get criterions
     */
    get critery()
    {
        return this.criterions;
    }
}