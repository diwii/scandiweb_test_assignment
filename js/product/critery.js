class Critery
{
    name;
    argument;
    message;

    constructor(name, argument, message)
    {
        this.name = name;
        this.argument = argument;
        this.message = message;
    }

    /**
     * Check criterions
     */
    check(input, productPropertyObj)
    {
        // Call critery with input argument.
        if (this[this.name](input)) return true;
        return false;
    }
    /**
     * Check whether input.value exists in array;
     * Argument should be array of strings.
     */
    unique(input)
    {
        let found = this.argument.find(element => {
            return element.toLowerCase() === input.value.toLowerCase();
        });
        if (found) {
            input.setCustomValidity(this.message);
            return false;
        }
        return true;
    }
    /**
     * Check if is set
     */
    required(input)
    {
        input.setCustomValidity(this.message);

        if (input.value.length === 0) {
            input.setCustomValidity(this.message);
            return false
        }
        // input.setCustomValidity('');
        return true;
    }
    notBeginWith(input)
    {
        if (input.value.startsWith(this.argument)) {
            input.setCustomValidity(this.message);
            return false;
        }
        return true;
    }
    
    match(input)
    {
        return true; // add code here
    }
}