<?php

namespace App;

class Request {

    function __construct()
    {
        $this->bootstrapSelf();
    }

    /**
     * Create and set request properties.
     */
    private function bootstrapSelf()
    {
        foreach($_SERVER as $key => $value) {
            $this->{$this->toCamelCase($key)} = $value;
        }
    }

    /**
     * Format property names toCamelCase.
     */
    private function toCamelCase($string)
    {
        $result = strtolower($string);
        // Perform regex to find underscore and letter next to it.
        preg_match_all('/_[a-z]/', $result, $matches);

        foreach ($matches[0] as $match) {
            // Remove underscore and set letter to uppercase.
            $char = str_replace('_', '', strtoupper($match));
            // Replace matched string with formated char.
            $result = str_replace($match, $char, $result);
        }
        
        return $result;
    }

    /**
     * Return request body.
     */
    public function getBody()
    {
        if ($this->requestMethod === 'GET') {
            return;
        }

        // If Json POST
        if(strcasecmp($this->contentType, 'application/json') === 0) {
            // Read the input stream.
            $json = trim(file_get_contents('php://input'));

            // Decode the JSON object to array and return.
            $body = json_decode($json, true);

            return $this->sanitize($body);
        }

        // If POST request.
        return $this->sanitize($_POST);
    }

    /**
     * Sanitize request body.
     */
    public function sanitize($input)
    {
        $clean = [];
        foreach ($input as $key => $dirty){
            $dirty = trim($dirty);
            $dirty = strip_tags($dirty);
            $dirty = stripslashes($dirty);
            $dirty = htmlspecialchars($dirty, ENT_QUOTES, 'UTF-8');
            $clean[$key] = $dirty;
        }

        return $clean;
    }
}