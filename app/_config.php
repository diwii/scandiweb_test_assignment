<?php

namespace App;

class Config {
    const HOST = 'localhost';
    const DB_NAME = 'db_name';
    const PASS = 'db_pass';
    const USER = 'db_user';
    const CHARSET = 'utf8mb4';
}