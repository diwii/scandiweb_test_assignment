<?php

namespace App;

class Route {

    /**
     * Request object.
     */
    private $request;

    /**
     * Supported HTTP methods.
     */
    private $supportedHttpMethods = [
        "get",
        "post"
    ];

    function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Create route method dictionary.
     * route->method[uri] = closure.
     * 
     * __call() is triggered when invoking inaccessible methods in an object context.
    */
    function __call($methodName, $args)
    {
        // Deconstruct array
        list($uri, $closure) = $args;

        // this->{methodName}[uri] = value
        // this->get[uri] = method || this->post[uri] = closure.
        $this->{strtolower($methodName)}[$this->formatUri($uri)] = $closure;
    }
    
    /**
     * Remove slashes, whitespace from the end of a uri.
     */
    private function formatUri($uri)
    {
        $result = rtrim($uri, '/');

        if ( $result === '' ) {
            return '/';
        }

        return $result;
    }

    /**
     * If http request method not supported.
     */
    private function invalidMethodHandler()
    {
        header("{$this->request->serverProtocol} 405 Method not Allowed");
    }

    /**
     * If uri not found.
     */
    private function defaultRequestHandler()
    {
        header("{$this->request->serverProtocol} 404 Not Found");
    }

    /**
     * Resolve requested uri.
     */
    function resolve() {

        $requestMethod = strtolower($this->request->requestMethod);

        // Check whether http request method is in supported route method list.
        if( !in_array($requestMethod, $this->supportedHttpMethods) ) {
            $this->invalidMethodHandler();
            return;
        }

        // Array of uris with associated method.
        $methodDictionary = $this->{$requestMethod};
        $requestUri = $this->formatUri($this->request->requestUri);

        // Check whether request uri exists in route->{method}[uri]
        if (array_key_exists($requestUri, $methodDictionary) ) {
            $closure = $methodDictionary[$requestUri];
        } else {
            $this->defaultRequestHandler();
            return; 
        }

        // Call $closure with request as argument.
        call_user_func_array($closure, array($this->request));
    }

    function __destruct() {
            
        $this->resolve();
    }

}