<?php

namespace App\Product;

class Book extends Product
{
    public $bookWeight;
    public $type = "book";
    /**
     * Allowed fields that will be inserted in database.
     */
    protected $childFillables = ['id', 'bookWeight'];
}