<?php

namespace App\Product;

use App\Mysql;
use App\Product\ProductTables;

abstract class Product
{
    public $id;
    public $sku;
    public $name;
    public $price;

    /**
     * Allowed fields that will be inserted in database.
     * Maybe it is possible to use ReflectionClass to manage this.
     * (new \ReflectionObject($this))->getProperties(\ReflectionProperty::IS_PUBLIC)
     */
    private $parentFillables = ['sku','name','price','type'];
    protected $childFillables = array();

    /**
     * Database connection instance.
     */
    private $db;

    /**
     * Table name is equal to Class name, from where the call is made.
     */
    private $tableName;

    /**
     * This name is used in database as common table, in this case it is Product.
     */
    private $parentTableName;

    public function __construct()
    {
        $this->db = $this->dbConnect();
        // Get caller class name, remove namespace and set as $tableName.
        $this->tableName = $this->tableName(static::class);
        // Abstract class name will be used as main table name in database in this case: Product table.
        $this->parentTableName = $this->tableName(self::class);
    }

    /**
     * Connect to database
     */
    private static function dbConnect()
    {
        return Mysql::connect();
    }

    /**
     * Return table name from class name
     */
    private static function tableName($className)
    {
        $tableName = str_replace(__NAMESPACE__ . '\\', '', $className);
        return $tableName;
    }

    /**
     * Returns Product child object from type passed in.
     */
    public static function create($type)
    {
        $type = ucfirst(strtolower($type));

        $className = __NAMESPACE__."\\{$type}";
        if (class_exists($className)) return new $className;

        throw new \Exception('Class not found'); // Should I do this?
    }

    /**
     * Set product values from an array passed in.
     */
    public function setValues($productPropValues)
    {
        $properties = (get_object_vars($this));

        foreach (array_keys($properties) as $propName) {
            if (!array_key_exists($propName, $productPropValues)) continue;

            $this->$propName = $productPropValues[$propName];
        }
        
        return $this;
    }

    /**
     * Store product in database.
     */
    public function store()
    {
        // Columns for db query.
        $parentColumnNames = implode(',', $this->parentFillables);
        $childColumnNames = implode(',', $this->childFillables);

        // Placeholders for db query.
        $parentPlaceHolders = $this->getPlaceHolders($this->parentFillables);
        $childPlaceHolders = $this->getPlaceHolders($this->childFillables);

        // Values for db query.
        $parentValues = array_intersect_key(get_object_vars($this), array_flip($this->parentFillables));
        $childValues = array_intersect_key(get_object_vars($this), array_flip($this->childFillables));

        try {
            $this->db->beginTransaction();
        
            // Insert Product values.
            $sql = "INSERT INTO {$this->parentTableName} ({$parentColumnNames})
                        VALUES ({$parentPlaceHolders})";
            $stmt = $this->db->prepare($sql)->execute($parentValues);
        
            $this->id = $this->db->lastInsertId(); // Get inserted product id
            $childValues['id'] = $this->id;
        
            // Insert product Type values.
            $sql = "INSERT INTO {$this->tableName} ({$childColumnNames})
                        VALUES ({$childPlaceHolders})";
            $stmt = $this->db->prepare($sql)->execute($childValues);
        
            $this->db->commit();
        } catch (\PDOException $e) {
            $this->db->rollback();
            // echo $e->getMessage();
            
            return false;
        }

        return $this;
    }

    /**
     * From array to string with prefix (:) and suffix (,).
     * Return string with placeholders :attribute1,:attribute2
     */
    private function getPlaceHolders($fillables)
    {
        // Add prefix(:) and suffix(,) to a placeholder.
        $placeHolders = array_map(function($value)
        {
            return ":{$value},";
        }, $fillables);

        // To string.
        $placeHolders = implode($placeHolders);

        // Remove last comma.
        $placeHolders = substr($placeHolders, 0,-1);
        
        return $placeHolders;
    }

    /**
     * Return all records from databse.
     */
    public static function all()
    {
        $db = self::dbConnect();
        // Get table name based on class name.
        $parentTable = self::tableName(self::class);

        try {
            // Return unique product types.
            $sql = "SELECT DISTINCT type FROM {$parentTable}";
            $type = $db->query($sql)->fetchAll(\PDO::FETCH_COLUMN);
        } catch (\PDOException $e) {
            echo $e->getCode();
            if ($e->getCode() == '42S02') {
                echo ' Product tables missing. Attempting to create product tables.';
                ProductTables::create();
                exit;
            }
        }

        // Holds product arrays.
        $products = [];
        // Foreach type create query to get product of type.
        foreach($type as $typeName) {
            $childTable = ucfirst($typeName);
            try {
                // Select product records and join with product type table.
                $sql = "SELECT * FROM {$parentTable}
                        INNER JOIN {$childTable}
                        ON {$parentTable}.id = {$childTable}.id";

                $productsOfType = $db->query($sql);

                // Pushes returned record to products list.
                foreach ($productsOfType as $product) {
                    array_push($products, $product);
                }
            } catch (\PDOException $e) {
                echo $e->getMessage();
            }
        }

        // Sort, latest first.
        usort($products, function($a, $b) {
            $a = strtotime($a['time']);
            $b = strtotime($b['time']);
            // return $a - $b;
            return $b - $a;
        });

        return $products;
    }

    /**
     * Check whether record exists with given column value.
     * Returns bool.
     */
    public static function exists($column, $value)
    {
        $db = self::dbConnect();
        // Get table name based on class name.
        $tableName = self::tableName(self::class);
        try {
            // Check if exists
            $sql = "SELECT EXISTS(SELECT 1 FROM {$tableName} WHERE {$column} = ?)";
            $stmt = $db->prepare($sql);
            $stmt->execute([$value]);
            $exists = $stmt->fetch(\PDO::FETCH_COLUMN);
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }

        if ($exists === '1') return true;
        return false;
    }

    /**
     * Delete records from list of ids.
     */
    public static function massDelete($list)
    {
        $db = self::dbConnect();
        // Get table name based on class name.
        $tableName = self::tableName(self::class);
        $deleted; // Row Count.

        try {
            // Create placeholders for list of ids.
            $placeholders  = str_repeat('?,', count($list) - 1) . '?';
            // Delete records
            $sql = "DELETE FROM {$tableName} WHERE id IN ({$placeholders})";
            $stmt = $db->prepare($sql);
            $stmt->execute($list);
            $deleted = $stmt->rowCount();
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }

        return $deleted;
    }
}