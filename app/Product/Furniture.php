<?php

namespace App\Product;

class Furniture extends Product
{
    public $furnitureHeight, $furnitureWidth, $furnitureDepth;
    public $type = "furniture";
    /**
     * Allowed fields that will be inserted in database.
     */
    protected $childFillables = ['id', 'furnitureHeight', 'furnitureWidth', 'furnitureDepth'];
}