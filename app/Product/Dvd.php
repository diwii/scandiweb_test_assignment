<?php

namespace App\Product;

class Dvd extends Product
{
    public $dvdSize;
    public $type = "dvd";
    /**
     * Allowed fields that will be inserted in database.
     */
    protected $childFillables = ['id', 'dvdSize'];
}