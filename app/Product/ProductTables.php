<?php

namespace App\Product;

use App\Mysql;

class ProductTables {
    /**
     * Connect to database
     */
    private static function dbConnect()
    {
        return Mysql::connect();
    }

    public static function create()
    {
        $db = self::dbConnect();

        try {
            $db->exec("CREATE TABLE IF NOT EXISTS Product (
                id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
                sku VARCHAR(20) NOT NULL UNIQUE,
                name VARCHAR(50) NOT NULL,
                price FLOAT NOT NULL,
                type VARCHAR(20) NOT NULL,
                time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP)");
        } catch (PDOException $e) {
            echo $e->getMessage();
        }


        try {
            $db->exec("CREATE TABLE IF NOT EXISTS Dvd (
                id INTEGER NOT NULL,
                dvdSize FLOAT NOT NULL,
                FOREIGN KEY (id) REFERENCES Product(id) ON DELETE CASCADE
            )");
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

        try {
            $db->exec("CREATE TABLE IF NOT EXISTS Book (
                id INTEGER NOT NULL,
                bookWeight FLOAT NOT NULL,
                FOREIGN KEY (id) REFERENCES Product(id) ON DELETE CASCADE
            )");
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

        try {
            $db->exec("CREATE TABLE IF NOT EXISTS Furniture (
                id INTEGER NOT NULL,
                furnitureWidth FLOAT NOT NULL,
                furnitureHeight FLOAT NOT NULL,
                furnitureDepth FLOAT NOT NULL,
                FOREIGN KEY (id) REFERENCES Product(id) ON DELETE CASCADE
            )");
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}