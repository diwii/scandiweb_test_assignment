<?php

namespace App;

class Response
{
    /**
     * Status code.
     */
    private $status = 200;

    /**
     * Set status code.
     */
    public function status(int $code)
    {
        $this->status = $code;
        return $this;
    }

    /**
     * Respond in json format.
     */
    public function toJson($payload)
    {
        http_response_code($this->status);
        header("content-type: application/json");
        echo json_encode($payload);
        exit;
    }

    /**
     * Respond in text/html content.
     */
    public function toHtml($payload)
    {
        http_response_code($this->status);
        header("content-type: text/html");
        echo $payload;
        exit;
    }

    /**
     * Redirect to uri.
     * Set header "Location: uri".
     */
    public function redirect($location)
    {
        http_response_code($this->status);
        header("Location: {$location}");
        exit;
    }

    /**
     * Response without conent.
     */
    public function noContent()
    {
        http_response_code($this->status);
        exit;
    }
}