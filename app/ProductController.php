<?php

namespace App;

use App\Response;
use App\Product\Product;
use App\Validator\Validator;
use App\Validator\Rule;

class ProductController
{
    /**
     * Product list.
     */
    public static function index()
    {
        // Set document title.
        $document = ['title' => 'Product List'];
        // Set document body title.
        $body = ['title' => 'Product List'];
        // Get product list.
        $products = Product::all();

        // Encode HTML special chars.
        array_walk_recursive($products, function(&$value){
            $value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
        });
        
        $view = new View('product.index', compact('document', 'body', 'products'));
        $view->output();
        exit;
    }

    /**
     * All product Json.
     */
    public static function Json()
    {
        $response = new Response;
        $products = Product::all();
        $response->status(200)->toJson($products);
        exit;
    }

    /**
     * Product creation form.
     */
    public static function create()
    {
        $document = ['title' => 'Product Add'];
        $body = ['title' => 'Product Add'];
        $form = ['attribute' => [
                    'action' => '/product/add',
                    'method' => 'POST']
                ];
        $view = new View('product.create', compact('document', 'body', 'form'));
        $view->output();
        exit;
    }

    /**
     * Validate, Store product in database.
     */
    public static function store($request)
    {
        $response = new Response;
        $request = $request->getBody();
        
        // Validation Rules.
        $fields = [
            'sku' => [
                new Rule('unique'),
                new Rule('maxLength', 20),
                new Rule('regexNot', '^0', 'This field should not start with 0'),
                new Rule('required')
            ],
            'name' => [
                new Rule('maxLength', 50),
                new Rule('required')
            ],
            'price' => [
                new Rule('float'),
                new Rule('positive'),
                new Rule('required')
            ],
            'type' => [
                new Rule('maxLength', 20),
                new Rule('oneOf', 'dvd,furniture,book') // Requires to be one of type.
            ],
            'dvdSize' => [
                new Rule('float'),
                new Rule('positive'),
                new Rule('requiredIf', 'type:dvd')
            ],
            'bookWeight' => [
                new Rule('float'),
                new Rule('positive'),
                new Rule('requiredIf', 'type:book')
            ],
            'furnitureHeight' => [
                new Rule('float'),
                new Rule('positive'),
                new Rule('requiredIf', 'type:furniture')
            ],
            'furnitureWidth' => [
                new Rule('float'),
                new Rule('positive'),
                new Rule('requiredIf', 'type:furniture')
            ],
            'furnitureDepth' => [
                new Rule('float'),
                new Rule('positive'),
                new Rule('requiredIf', 'type:furniture')
            ]
        ];

        $validator = new Validator($request, $fields);
        $validator->run();

        // If validator returns no errors.
        if ($validator->errors() === null) {

            $validatedProd = $validator->validated;
            $product = Product::create($validatedProd['type'])->setValues($validatedProd);

            // Check if actually stored in db.
            if (!$product->store()) {
                $response->status(501)->noContent();
                exit;
            }

            $response->status(201)->redirect('/product/list');
            exit;
        }
        
        // If validation fails respond with validator errors, old values.
        $data = [
            'error' => $validator->errors(),
            'old' => $request // old values.
            ];

        $response->status(200)->toJson($data);
        exit;
    }

    /**
     * Perform mass delete.
     * Requires array of record ids to delete.
     */
    public static function massDelete($request)
    {
        $response = new Response;
        // Array of ids to delete.
        $list = $request->getBody();

        if (empty($list)) {
            $response->status(200)->toHtml('List for deletion is empty');
            exit;
        } else {
            $result = Product::massDelete($list);
            if ($result > 0) {
                $response->status(204)->noContent();
                exit;
            } else {
                $response->status(200)->toHtml('Something went wrong with deletion.');
                exit;
            }
        }
    }
}