<?php
/**
 * Singleton pattern for Mysql database connection
 */
namespace App;

require_once('_config.php');

class Mysql {

    /**
     * Error handling Exceptions.
     */
    private static $options = [
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
    ];

    /**
     * Hold the database connection.
     */
    private static $dbConnection = null;

    private function __construct() {
    }
    private function __clone() {
    }

    public static function connect() {
        // If there isn't a connection already then create one
        if ( self::$dbConnection == null ) {
            try {
                // Data source name
                $DSN = "mysql:host=".Config::HOST.";dbname=".Config::DB_NAME.";charset=".Config::CHARSET;
                // Connect to data source
                self::$dbConnection = new \PDO($DSN, Config::USER, Config::PASS, self::$options);
            }
            catch( \PDOException $e ) {
                echo ':(';
                exit;
            }
        }
        return self::$dbConnection;
    }
}