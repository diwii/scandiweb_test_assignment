<?php

namespace App\Validator;

use App\Validator\Validable;

class Validator
{
    /**
     * Holds Request.
     */
    public $request;

    /**
     * Array of fields that will be validated.
     */
    public $validables = [];

    /**
     * Array of fields after validation.
     * If field didn't pass validation its value is null.
     */
    public $validated = [];

    /**
     * Error bag.
     */
    public $errors = [];

    public function __construct($request, $validables)
    {
        $this->request = $request;
        $this->setValidables($validables);
    }

    /**
     * Fill array of fields to be validated.
     */
    private function setValidables($validables)
    {
        foreach ($validables as $name => $rules) {       
            array_push($this->validables, new Validable($name, $rules));
        }
    }

    /**
     * Run validation loop.
     */
    public function run()
    {
        foreach ($this->validables as $validable) {
            $validable->validate($this->request, $this->validated);

            // Sets validable to validated, it's value may be null or "".
            $this->validated[$validable->fieldName] = $validable->value;
            
            // Sets field error message.
            $this->errors[$validable->fieldName] = $validable->errors;
        }

        return $this;
    }
    
    /**
     * Loop through Error bag for each field.
     */
    public function errors()
    {
        $errorBag = [];
        foreach ($this->errors as $fieldName => $error) {
            if ( count($error) === 0 ) continue;

            $errorBag[$fieldName] = $error;
        }

        if (count($errorBag) === 0) return null;
        return $errorBag;
    }
}