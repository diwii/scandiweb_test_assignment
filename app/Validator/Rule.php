<?php

namespace App\Validator;

use App\Product\Product;

class Rule
{
    /**
     * Rule name.
     */
    public $name;

    /**
     * Argument for rule to operate on.
     */
    public $argument;

    /**
     * Validation message.
     */
    public $message;

    /**
     * Hold validated fields.
     */
    private $validated;

    /**
     * Hold field that is being validated.
     */
    private $validable;

    public function __construct($name, $argument=null, $message=null)
    {
        $this->name = $name;
        $this->argument = $argument;
        $this->message = $message;
    }

    private function setMessage(string $message)
    {
        if (isset($this->message)) return;
        $this->message = $message;
    }

    /**
     * Check request value.
     * Call rule method, whose name is obtained from rule name.
     * Pass in $validable object,
     * Pass in $validated field array.
     */
    public function check($value, $validable, $validated)
    {
        $this->validated = $validated;
        $this->validable = $validable;

        $methodName = $this->name;
        return $this->$methodName($value);
    }

    public function required($value)
    {
        if (strlen($value) === 0) {
            $this->setMessage('This field is required');
            return false;
        }
        return true;
    }

    /**
     * Required if some other key exists.
     * Rule argument should contain key : value pair,
     * example: fieldName:vale,
     * example: type:dvd
     */
    public function requiredIf($value)
    {
        //group1 is key : group2 is value
        $pattern = "/(\w+):(\w+)/";
        preg_match($pattern, $this->argument, $group);

        if ($this->validated[$group[1]] === $group[2]) {
            return $this->required($value);
        }
        return true;
    }
    
    /**
     * Field must be one of passed arguments.
     */
    public function oneOf($value)
    {
        $viables = explode(',', $this->argument);
        foreach ($viables as $viable) {
            if ($value === $viable) return true;
        }
        $this->setMessage('This field must be one of '.$this->argument.'.');
        return false;
    }

    public function unique($value)
    {
        $column = $this->validable->fieldName;
        if (!Product::exists($column, $value)) return true;
        $this->setMessage('This field is not unique');
        return false;
    }

    /**
     * Check for regex, return true if pattern not match.
     */
    public function regexNot($value)
    {
        $pattern = '/'.$this->argument.'/';
        if ( !preg_match($pattern, $value) ) return true;
        $this->setMessage('This field should not match this pattern: '.$this->argument);
        return false;
    }

    /**
     * Check wheter value is of type float or 0.
     */
    public function float($value)
    {
        if (strlen($value) === 0 || $value == 0) return true; // Skip validation if field empty or 0.
        if (filter_var($value, FILTER_VALIDATE_FLOAT)) return true;
        $this->setMessage('This field is not of type float');
        return false;
    }

    /**
     * Check if value is positive.
     */
    public function positive($value)
    {
        if (strlen($value) === 0 || $value == 0) return true; // Skip validation if field empty or 0.
        if ($value >= 0) return true;
        $this->setMessage('This field must be positive.');
        return false;
    }

    public function maxLength($value)
    {
        if (strlen($value) <= $this->argument) return true;
        $this->setMessage("This field must be less than or equal to {$this->argument} characters long");
        return false;
    }
}