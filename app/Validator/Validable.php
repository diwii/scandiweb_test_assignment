<?php

namespace App\Validator;

class Validable
{
    /**
     * Field name.
     */
    public $fieldName;

    /**
     * Field value.
     * Is set when no validation errors accured.
     */
    public $value = null;

    /**
     * Array of rule objects to be validated against.
     */
    public $rules = [];

    /**
     * Holds validation error messages.
     */
    public $errors = [];

    public function __construct($fieldName, $rules)
    {
        $this->fieldName = $fieldName;
        $this->rules = $rules;
    }

    /**
     * Validate field for each rule.
     * Pass in $request, $validated fields.
     */
    public function validate($request, $validated)
    {
        foreach ($this->rules as $rule) {
            // Check request value against rule.
            if ($rule->check($request[$this->fieldName] ?? "", $this, $validated)) continue;
            
            // If validation fails, push rule validation error message.
            array_push($this->errors, $rule->message);
        }

        // Set value
        if (count($this->errors) === 0) {
            $this->value = $request[$this->fieldName] ?? '';
        }

        return $this;
    }
}