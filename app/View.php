<?php

namespace App;

class View
{
    /**
     * View file path settings.
     */
    private $settings = [
        'baseDir' => 'views',
        'prefix' => '/',
        'viewName' => null,
        'suffix' => '.view.php'
    ];
    
    /**
     * Holds data that will be passed to output.
     */
    private $data;

    public function __construct($viewName, $data=null)
    { 
        $this->resolve($viewName);
        $this->data = $data;
    }

    /**
     * Update settings with view file path.
     */
    private function resolve($path)
    {
        // Split string into array.
        $path = explode('.', $path);

        // Set the View name from last element of $path array.
        $this->settings['viewName'] = array_pop($path);

        // Set the prefix(a directory) of view filename.
        if (!empty($path)) {
            $this->settings['prefix'] = '/'.implode('/', $path).'/';
        }
    }

    public function output()
    {
        if (is_array($this->data)) {
            extract($this->data);
        }

        // Get header // This needs better solution.
        include_once $this->settings['baseDir'].'/includes/header'.$this->settings['suffix'];
        // Get body
        include_once implode('', $this->settings);
        // Get footer // This needs better solution.
        include_once $this->settings['baseDir'].'/includes/footer'.$this->settings['suffix'];
    }
}