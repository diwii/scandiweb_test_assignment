<main>
    <!-- Page Header -->
    <header class="container">
        <!-- Page Title -->
        <h1 class="page-title"><?= $body['title'] ?? ""; ?></h1>
        <!-- Page Navigation -->
        <nav>
            <button type="button" id="save" class="button" tabindex="10">Save</button>
            <a href="/product/list" class="button" tabindex="11">Cancel</a>
        </nav>
    </header>

    <form id="createProduct" action="<?= $form['attribute']['action'] ?? ""; ?>" method="<?= $form['attribute']['method'] ?? "" ?>">

    <!-- READ THIS -->
    <!-- https://simplyaccessible.com/article/error-messages-in-labels/ -->
        <label for="sku">SKU:</label>
        <input type="text"
                id="sku"
                name="sku"
                value=""
                maxlength="20"
                required
                tabindex="1"
                >
        <label for="sku" class="error"></label>

        <label for="name">Name:</label>
        <input  type="text" 
                id="name"
                name="name"
                value=""
                maxlength="50"
                required
                tabindex="2"
                >
        <label for="name" class="error"></label>

        <label for="price">Price ($)</label>
        <input  type="number"
                min="0"
                step=".01"
                id="price"
                name="price"
                value=""
                required
                tabindex="3"
                >
        <label for="price" class="error"></label>

        <label for="productType">Product type</label>
        <select id="productType" name="type" tabindex="4">
            <option value="" selected disabled hidden></option>
            <option value="dvd">DVD-disc</option>
            <option value="book">Book</option>
            <option value="furniture">Furniture</option>
        </select>
        <label for="productType" class="error"></label>

        <fieldset id="dvd" style="display:none">
            <label for="dvdSize">Size:</label>
            <input  type="number"
                    min="0"
                    step=".01"
                    id="dvdSize"
                    name="dvdSize"
                    value=""
                    required
                    tabindex="5"
                    disabled
                    >
            <label for="dvdSize" class="error"></label>
            <em class="description">Please provide size in megabytes.</em>
        </fieldset>

        <fieldset id="book" style="display:none">
            <label for="bookWeight">Weight (KG)</label>
            <input  type="number"
                    min="0"
                    step=".01" 
                    id="bookWeight"
                    name="bookWeight"
                    value=""
                    required
                    tabindex="6"
                    disabled
                    >
            <label for="bookWeight" class="error"></label>
            <em class="description">Please provide weight in kilograms.</em>
        </fieldset>

        <fieldset id="furniture" style="display:none">
            <label for="furnitureHeight">Height (CM)</label>
            <input  type="number"
                    min="0"
                    step=".01"
                    id="furnitureHeight"
                    name="furnitureHeight"
                    value=""
                    required
                    tabindex="7"
                    disabled
                    >
            <label for="furnitureHeight" class="error"></label>

            <label for="furnitureWidth">Width (CM)</label>
            <input  type="number"
                    min="0"
                    step=".01" 
                    id="furnitureWidth"
                    name="furnitureWidth"
                    value=""
                    required
                    tabindex="8"
                    disabled
                    >
            <label for="furnitureWidth" class="error"></label>

            <label for="furnitureDepth">Depth (CM)</label>
            <input  type="number" 
                    min="0"
                    step=".01"
                    id="furnitureDepth"
                    name="furnitureDepth"
                    value=""
                    required
                    tabindex="9"
                    disabled
                    >
            <label for="furnitureDepth" class="error"></label>
            <em class="description">Please provide dimensions in HxWxL format.</em>
        </fieldset>

        <input type="submit" value="Submit">

    </form>
</main>

<script src="/js/notification.js"></script>
<script src="/js/product/fields.js"></script>
<script src="/js/product/critery.js"></script>
<script src="/js/product/productProperty.js"></script>
<script src="/js/product/type.js"></script>
<script src="/js/product/product.js"></script>
<script src="/js/createProduct.js"></script>