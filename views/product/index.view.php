<main>
    <!-- Page Header -->
    <header class="container">
        <!-- Page Title -->
        <h1 class="page-title"><?= $body['title'] ?? ""; ?></h1>
        <!-- Page Navigation -->
        <nav>
            <a href="/product/add" class="button">Add Product</a>
            <button type="button" id="massDelete" class="button">Mass Delete</button>
        </nav>
    </header>

    <section class="product-list">
        <?php foreach ($products as $product): ?>
            <article id="record-<?=$product['id']?>" class="product container">
                <header>
                    <h2 class="product-title"><?=$product['name']?></h2>
                    <input type="checkbox" class="markOfDeath" value="<?=$product['id']?>">
                </header>
                <ul>
                    <li>
                        <?=$product['sku']?>
                    </li>
                    <li>
                        <?=number_format($product['price'],2)?> &dollar;
                    </li>
                    <?php switch($product['type']):
                        case 'dvd':?>
                            <li>
                                Size: <?=$product['dvdSize']?> MB
                            </li>
                        <?php break;?>
                        <?php case 'book':?>
                            <li>
                                Weight: <?=$product['bookWeight']?> KG
                            </li>
                        <?php break;?>
                        <?php case 'furniture':?>
                            <li>
                                Dimensions:
                                <?=$product['furnitureHeight']?>
                                x <?=$product['furnitureWidth']?>
                                x <?=$product['furnitureDepth']?>
                            </li>
                        <?php break;?>
                    <?php endswitch?>
                </ul>
                <!-- <p>3.99 </p> -->
            </article>
        <?php endforeach ?>
    </section>
</main>
<script src="/js/notification.js"></script>
<script src="/js/index.js"></script>