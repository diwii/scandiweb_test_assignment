<?php

require 'vendor/autoload.php';

use App\Route;
use App\Request;
use App\ProductController;

$route = new Route(new Request);

$route->get('/', function()
{
    header("Location: /product/list"); // Redirect browser
    exit;
});

$route->get('/product/list', function($request)
{
    return ProductController::index();
});

$route->get('/product/list/json', function()
{    
    return ProductController::Json();
});

$route->get('/product/add', function()
{    
    return ProductController::create();
});

$route->post('/product/add', function($request)
{   
    $data = ProductController::store($request);
});

$route->post('/product/massDelete', function($request)
{
    $data = ProductController::massDelete($request);
});
